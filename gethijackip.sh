#!/bin/bash
i=0
DNSHOST=31.13.95.38   #随便选一个已经被墙、且不提供DNS查询服务的域名，如facebook
QUERYURL="zh.wikipedia.org"        #随便选一个已经被墙的域名，二者可以一样
COUNTLIMIT=2000             #查询次数
STARTTIME=$(date -u +%Y%m%d%H%M%S)


while [ $i -lt $COUNTLIMIT ]
do
	dig @$DNSHOST "$QUERYURL" +short | grep -E '[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}' >> ./hijackiplog_$DNSHOST\_$QUERYURL\_$COUNTLIMIT\_$STARTTIME.txt &
  i=`expr $i + 1`
done

cat ./hijackiplog_$DNSHOST\_$QUERYURL\_$COUNTLIMIT\_$STARTTIME.txt | grep -v DiG >> ./gfw_hijack_ip.txt
sort -ub ./gfw_hijack_ip.txt | tee ./gfw_hijack_ip.txt